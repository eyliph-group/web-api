const Config = require('../config.json')
const fetch = require('node-fetch');

const league = {
    getLeagueBySummoner: (id, region = Config.defaultRouting) => {
        const apiURL = "/lol/league/v4/entries/by-summoner/"
        const regionURL = Config.routingValue.find(e => e.platform === region)

        if (regionURL === undefined) regionURL = Config.routingValue.find(e => e.platform === Config.defaultRouting)
        console.log(`fetching : https://${regionURL.host}${apiURL}${id}`)

        return (
            fetch(`https://${regionURL.host}${apiURL}${id}`, {
                method: 'GET',
                headers: {
                    "X-Riot-Token": Config.devAPIkey
                }
            })
            .then(res => res.json())
            .then(json => { return json })
        )
    }
}

module.exports = league