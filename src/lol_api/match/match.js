const Config = require('../config.json')
const fetch = require('node-fetch');

const match = {
    getMatchByMatchId: (matchID, region = Config.defaultRouting) => {
        const apiURL = "/lol/match/v4/matches/"
        const regionURL = Config.routingValue.find(e => e.platform === region)

        if (regionURL === undefined) regionURL = Config.routingValue.find(e => e.platform === Config.defaultRouting)
        console.log(`fetching : https://${regionURL.host}${apiURL}${matchID}`)

        return (
            fetch(`https://${regionURL.host}${apiURL}${matchID}`, {
                method: 'GET',
                headers: {
                    "X-Riot-Token": Config.devAPIkey
                }
            })
            .then(res => res.json())
            .then(json => { return json })
        )
    },
}

module.exports = match