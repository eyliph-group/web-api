const summoner = require('./summoner/summoner.js')
const league = require('./league/league.js')
const match = require('./match/match.js')

module.exports = {
    summoner,
    league,
    match,
}