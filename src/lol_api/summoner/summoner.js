const Config = require('../config.json')
const fetch = require('node-fetch');

const Season = require('../season.json')

const summoner = {
    getSummonerByName: (name, region = Config.defaultRouting) => {
        const apiURL = "/lol/summoner/v4/summoners/by-name/"
        const regionURL = Config.routingValue.find(e => e.platform === region)

        if (regionURL === undefined) regionURL = Config.routingValue.find(e => e.platform === Config.defaultRouting)
        console.log(`fetching : https://${regionURL.host}${apiURL}${encodeURI(name)}`)

        return (
            fetch(`https://${regionURL.host}${apiURL}${encodeURI(name)}`, {
                method: 'GET',
                headers: {
                    "X-Riot-Token": Config.devAPIkey
                }
            })
            .then(res => res.json())
            .then(json => { return json })
        )
    },

    getSummonerByID: (id, region = Config.defaultRouting) => {
        const apiURL = "/lol/summoner/v4/summoners/"
        const regionURL = Config.routingValue.find(e => e.platform === region)

        if (regionURL === undefined) regionURL = Config.routingValue.find(e => e.platform === Config.defaultRouting)
        console.log(`fetching : https://${regionURL.host}${apiURL}${id}`)

        return (
            fetch(`https://${regionURL.host}${apiURL}${id}`, {
                method: 'GET',
                headers: {
                    "X-Riot-Token": Config.devAPIkey
                }
            })
            .then(res => res.json())
            .then(json => { return json })
        )
    },

    getLivegameByID: (id, region = Config.defaultRouting) => {
        const apiURL = "/lol/spectator/v4/active-games/by-summoner/"
        const regionURL = Config.routingValue.find(e => e.platform === region) || Config.routingValue.find(e => e.platform === Config.defaultRouting)

        console.log(`fetching : https://${regionURL.host}${apiURL}${id}`)

        return (
            fetch(`https://${regionURL.host}${apiURL}${id}`, {
                method: 'GET',
                headers: {
                    "X-Riot-Token": Config.devAPIkey
                }
            })
            .then(res => res.json())
            .then(json => { return json })
        )
    },

    getHistoByID: (id, region = Config.defaultRouting, season, champion, beginIndex, endIndex, queue) => {
        const apiURL = "/lol/match/v4/matchlists/by-account/"
        const regionURL = Config.routingValue.find(e => e.platform === region) || Config.routingValue.find(e => e.platform === Config.defaultRouting)
        const query = []

        if (champion) query.push(`champion=${champion}`)
        if (beginIndex) query.push(`beginIndex=${beginIndex}`)
        if (endIndex) query.push(`endIndex=${endIndex}`)
        if (queue) query.push(`queue=${queue}`)
        if (season) {
            const beginTime = new Date(Season.find(e => e.id === parseInt(season, 10)).beginDate).getTime()
            query.push(`beginTime=${beginTime}`)
        } else {
            const beginTime = new Date(Season.find(e => e.id === 1)).getTime()
            query.push(`beginTime=${beginTime}`)
        }
        console.log(`fetching : https://${regionURL.host}${apiURL}${id}?${query.join('&')}`)

        return (
            fetch(`https://${regionURL.host}${apiURL}${id}?${query.join('&')}`, {
                method: 'GET',
                headers: {
                    "X-Riot-Token": Config.devAPIkey
                }
            })
            .then(res => res.json())
            .then(json => { return json })
        )
    },
}

module.exports = summoner