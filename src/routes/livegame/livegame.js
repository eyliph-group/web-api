const express = require('express');
const router = express.Router();

const api = require('../../lol_api/lol_api.js')

router.get('/:summonerID', async(req, res) => {
    const region = req.query.region
    var data = {}
    await api.summoner.getLivegameByID(req.params.summonerID, region).then(resp => data = resp)
    console.log(data)
    res.json(data)
});

module.exports = router;