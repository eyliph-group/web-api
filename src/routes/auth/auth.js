const express = require('express');
const router = express.Router();

const api = require('../../lol_api/lol_api.js')

router.get('/:summonerName', async(req, res) => {
    const region = req.query.region
    var data = {}
    await api.summoner.getSummonerByName(req.params.summonerName, region).then(resp => data = resp)
    console.log(data)
    res.json(data)
});

module.exports = router;