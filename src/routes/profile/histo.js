const express = require('express');
const router = express.Router();
const fs = require('fs')

const api = require('../../lol_api/lol_api.js')
const Stats = require('../../data/histo_stat.json');

const getHisto = async(accountID, { region, champion, season, queue }) => {
    var data = {}
    await api.summoner.getHistoByID(accountID, region, season, champion, 0, null, queue).then(resp => data = { pages: [resp], totalGames: resp.totalGames })

    while (data.pages.length < Math.trunc(data.totalGames / 100) + !!(data.totalGames % 100)) {
        await api.summoner.getHistoByID(accountID, region, season, champion, data.pages.length * 100, null, queue).then(resp => data.pages.push(resp))
    }

    return data
}

router.get('/:accountID', async(req, res) => {
    const data = await getHisto(req.params.accountID, req.query)

    console.log(data)
    res.json(data)
});

const getMatches = async(accountID, query) => {
    const histo = await getHisto(accountID, query)
    const matches = []

    histo.pages.forEach(e => {
        e.matches.forEach(m => matches.push(m))
    });

    matches.sort((a, b) => a.timestamp - b.timestamp)
    return matches
}

const computeMatch = (accountData, data, champion, queue, timestamp) => {
    const championData = accountData.queue[queue].find(e => e.id === champion) || { id: champion, win: 0, lose: 0, kda: [] }
    const winningTeam = data.teams.find(e => e.win === "Win").teamId
    const accountParticipant = data.participants.find(e => e.championId === champion)

    if (winningTeam === accountParticipant.teamId)
        ++championData.win
    else
        ++championData.lose
    const { kills, deaths, assists } = accountParticipant.stats
    championData.kda.push({ kills, deaths, assists })
    championData.lastUpdate = timestamp

    if (accountData.queue[queue].find(e => e.id == champion) === undefined) accountData.queue[queue].push(championData)
}

router.get('/stats/:accountID', async(req, res) => {
    const matches = await getMatches(req.params.accountID, req.query)
    const accountData = Stats.find(e => e.id === req.params.accountID) || { id: req.params.accountID, queue: { "0": [], "420": [], "440": [] } }

    for (let i = 0; i < matches.length; ++i) {
        const { gameId, champion, queue, timestamp } = matches[i]
        const championData = accountData.queue[queue === 420 || queue === 440 ? queue.toString() : "0"].find(e => e.id === champion) || { id: champion, win: 0, lose: 0, kda: [] }
        let data = {}

        if (championData.lastUpdate && timestamp <= championData.lastUpdate) continue
        await api.match.getMatchByMatchId(gameId, req.query.region).then(resp => data = resp)
        if (data.status && data.status.status_code === 429) {
            --i;
            await new Promise(r => setTimeout(r, 10000))
            continue
        }
        computeMatch(accountData, data, champion, queue === 420 || queue === 440 ? queue.toString() : "0", timestamp)
    }

    if (Stats.find(e => e.id === req.params.accountID) === undefined) Stats.push(accountData)

    fs.writeFileSync('./src/data/histo_stat.json', JSON.stringify(Stats))
        // if (req.query.queue && !req.query.champion) res.json(accountData.queue[req.query.queue === '420' || req.query.queue === '440' ? req.query.queue : "0"])
        // else if (req.query.queue && req.query.champion) res.json(accountData.queue[req.query.queue === '420' || req.query.queue === '440' ? req.query.queue : "0"])
        // else res.json(accountData)
});

module.exports = router