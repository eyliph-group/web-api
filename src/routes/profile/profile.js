const express = require('express');
const router = express.Router();

const api = require('../../lol_api/lol_api.js')

router.get('/:summonerName', async(req, res) => {
    const region = req.query.region
    var data = {}
    await api.summoner.getSummonerByName(req.params.summonerName, region).then(resp => data = resp)
    await api.league.getLeagueBySummoner(data.id, region).then(resp => data = {...data, league: resp })
    console.log(JSON.stringify(data, null, 2))
    res.json(data)
});

module.exports = router;