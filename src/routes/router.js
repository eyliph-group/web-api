const root = require('./root/root.js')
const auth = require('./auth/auth.js')
const profile = require('./profile/profile.js')
const rank = require('./profile/rank.js')
const histo = require('./profile/histo.js')
const livegame = require('./livegame/livegame.js')

module.exports = (app) => {
    app.use('/', root)
    app.use('/auth', auth)
    app.use('/profile', profile)
    app.use('/rank', rank)
    app.use('/hist', histo)
    app.use('/livegame', livegame)
};