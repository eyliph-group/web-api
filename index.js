const express = require('express')
const router = require('./src/routes/router.js')

app = express(),
    port = process.env.PORT || 8080;

app.listen(port);
console.log('Api started on: ' + port);

// Add headers
app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
    res.setHeader('Access-Control-Allow-Origin', 'https://senayora.github.io');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
    next();
});

router(app)